# aiCTXinterview

aiCTX interview take home task: Accomplish a filters system based on event driven.

# Compile instructor
gcc -std=c++11 -pthread -lstdc++ filter.cpp

# Note

Demo the following filters' topology graph.


    transform  --------->   add
               --------->   remove


Generate 3 event collections which including 3 events for each and push them into input queue in main thread, after that start 3 filter threads to process these events.
