#include <iostream>
#include <mutex>
#include <condition_variable>
#include <unistd.h>
#include <thread>
#include <deque>
#include <vector>

using namespace std;

struct Event {
    uint64_t id;
    uint32_t timestamp;
};

mutex printMutex;      // just for debug printing, can be removed in working environment

template <typename T>
class eventQueue {
    public:
        eventQueue():published_num(0),index(0),subscriber(0) //default constructor
        {};
        eventQueue(uint32_t sub):published_num(0),index(0),subscriber(sub) // constructor with subscriber
        {};
        deque<vector<T>> queue;
        condition_variable cv;
        mutex  queueMutex;
        uint32_t published_num;
        uint32_t index;
        uint32_t subscriber;
} ;


void Remove(Event * e){
    cout<<"Call remove function on event id "<<e->id<<" timestamp  "<<e->timestamp<<endl;
}
void Add(Event * e){
    cout<<"Call add function on event id "<<e->id<<" timestamp  "<<e->timestamp<<endl;
}
void TransForm(Event * e){
    cout<<"Call transform function on event id "<<e->id<<" timestamp  "<<e->timestamp<<endl;
}

template <typename T>
void startFilter(eventQueue<T> * inputQueue, eventQueue<T> * outputQueue, void (*processFunction) (Event *e))
{
    uint32_t filterIndex = 0;// the index of collection that the filter want
    while (1){
        unique_lock<mutex> inLock(inputQueue->queueMutex);
        while (inputQueue->queue.size() == 0 ) //inputQueue is empty, wait on cv
            inputQueue->cv.wait(inLock);
        if (filterIndex != inputQueue->index)
        /* the queue's current index is not the index of collection the filter want,
         * which means the filter has got this collection before,
         * so unlock mutex to give the queue to other filters which need this collection
         * and skip
         */
        {
            inLock.unlock();
            continue;
        }
        vector<T> inputCollection(inputQueue->queue.back()); //copy constructor to get event from global queue

        inputQueue->published_num++; //delivery collection successful, increase published count
        filterIndex ++;  //increase the index of collection which the filter want

        if (inputQueue->published_num == inputQueue->subscriber) {
        /* the queue has published one collection to all the subscriber,
         * so increase the index to move the pointer to the next collection in queue
         * and clean the published count
         */
            inputQueue->index ++ ;
            inputQueue->published_num = 0;
            inputQueue->queue.pop_back();
        }
        inLock.unlock();
        vector<T> outputCollection;
        typename vector<T>::iterator it = inputCollection.begin();
        for (;it != inputCollection.end();++it){
            T event = *it;
            {
                lock_guard<mutex> printMtx (printMutex);   //prevent printing overlapped
                //cout<<event.id<<"  "<<event.timestamp<<"  at "<<this_thread::get_id()<<endl;
                (*processFunction)(&event);       //call process function by function pointer
            }

            outputCollection.push_back(event);

        }

        unique_lock<mutex> outLock(outputQueue->queueMutex);
        outputQueue->queue.push_front(outputCollection);
        outputQueue->cv.notify_all();        // notify filters which wait for input that input is ready
        outLock.unlock();
    }
}

int main()
{
    eventQueue<Event> inputQ(1),outputQ(2);
    eventQueue<Event> outputQ2, outputQ3;

    vector<Event> collection1;
    collection1.push_back({1,1});
    collection1.push_back({1,2});
    collection1.push_back({1,3});

    vector<Event> collection2;
    collection2.push_back({2,1});
    collection2.push_back({2,2});
    collection2.push_back({2,3});

    vector<Event> collection3;
    collection3.push_back({3,1});
    collection3.push_back({3,2});
    collection3.push_back({3,3});
    inputQ.queue.push_front(collection1);
    inputQ.queue.push_front(collection2);
    inputQ.queue.push_front(collection3);

    thread t1(startFilter<Event>,&inputQ,&outputQ, TransForm);
    thread t2(startFilter<Event>,&outputQ,&outputQ2, Add);
    thread t3(startFilter<Event>,&outputQ,&outputQ3, Remove);
    t1.join();
    t2.join();
    t3.join();

    cout <<"End"<<endl;
    return 0;

}
